{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE MonoLocalBinds #-}
module Main where

import Optics

main :: IO ()
main = putStrLn "Hello, Haskell!"

g :: Lens' String Char                                                                                   
g = undefined                                                                                            

f :: _
f o = "foo" & (g%o) .~ 10
